// Fill out your copyright notice in the Description page of Project Settings.


#include "STZoneClientComponent.h"
#include <ST/ZonaFighting/STZonaFighting.h>


void USTZoneClientComponent::OnZoneSwitched(bool bIsOn)
{
	FString OnOffString = bIsOn ? TEXT("ON") : TEXT("OFF");
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, FString::Printf(TEXT("+++")));

	if (ZoneClientOnSwitchedSignature.IsBound())
	{
		ZoneClientOnSwitchedSignature.Broadcast(bIsOn);
	}
}

// Called when the game starts
void USTZoneClientComponent::BeginPlay()
{
	Super::BeginPlay();


	if (IsValid(ZonaFight))
	{
		ZonaFight->OnZonaFightingSwitchedSignature.AddUObject(this, &USTZoneClientComponent::OnZoneSwitched);

	}
}

