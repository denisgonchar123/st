// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "STZoneClientComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FZoneClientOnSwitchedSignature, bool, bIsOn);


class ASTZonaFighting;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ST_API USTZoneClientComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	
	UPROPERTY(EditAnywhere)
	ASTZonaFighting* ZonaFight;

	UPROPERTY(BlueprintAssignable)
	FZoneClientOnSwitchedSignature ZoneClientOnSwitchedSignature;

	UFUNCTION()
	void OnZoneSwitched(bool bIsOn);
	

	virtual void BeginPlay() override;


		
};
