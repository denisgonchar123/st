// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include <ST/Weapons/WeaponDefault.h>
#include <ST/Types/Types.h>
#include "../TimeComponent/STTimeComponent.h"
#include "STCharacter.generated.h"

class USTCharacterMovementComponent;

UCLASS(Blueprintable)
class ASTCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ASTCharacter(const FObjectInitializer& ObjectInitializer);

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	
	FORCEINLINE class USTCharacterMovementComponent* GetBaseCharacterMovementComponent() { return BaseCharacterMovementComponent; }

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USTInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USTTimeComponent* TimeComponent;
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UDecalComponent* CurrentCursor = nullptr;
	
	

	USTCharacterMovementComponent* BaseCharacterMovementComponent;

public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	TArray<UAnimMontage*>DeadsAnim;

	//Tick Func
	UFUNCTION()
	void MovementTick(float DeltaTime);
	void TryChangeSprintState(float DeltaTime);
	void TryChangeWalkState(float DeltaTime);

	//Move
	virtual void MoveForward(float Value) { MoveForwardX = Value; }
	virtual void MoveRight(float Value) { MoveRightY = Value; };
	float MoveForwardX = 0.0f;
	float MoveRightY = 0.0f;

	//Fire
	void InputAttackPressed();
	void InputAttackReleased();
	void AttackCharEvent(bool bIsFiring);

	//Sprint
	virtual void StartSprint();
	virtual void EndSprint();
	virtual bool CanSprint();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sprint")
	bool bIsCanSprint = true;

	//Walk
	virtual void StartWalk();
	virtual void EndWalk();
	virtual bool CanWalk();
	
	//Weapon
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	int32 CurrentIndexWeapon = 0;

	UPROPERTY(editAnywhere, BlueprintReadWrite, category = "Demo")
	FName InitWeaponName;

	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();

	//Anim
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	
	//Interface
	void TrySwicthNextWeapon();
	void TrySwicthPreviosWeapon();

	//Character dead
	UFUNCTION(BlueprintCallable)
	void CharDead();
	void EnableRandoll();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	bool Sword = false;

	UFUNCTION(BlueprintCallable)
	bool GetSword() { return Sword; }

protected:
	//Arm Length
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ArmLength")
	bool bIsPlayingDistanceArmLength = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ArmLength")
	float PlayingDistanceArmLength = 1800.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ArmLength")
	float ProgramDistanceArmLength = 800.0f;
	
	//Sprint
	bool bIsSprintRequested = false;
	//Walk
	bool bIsWalkRequested = false;
	//Reload
	bool bIsReload = false;


};

