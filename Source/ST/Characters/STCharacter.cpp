// Copyright Epic Games, Inc. All Rights Reserved.

#include "STCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include <ST/MovementComponents/STCharacterMovementComponent.h>
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>
#include <ST/GameInstance/STGameInstance.h>
#include <ST/InventoryComponent/STInventoryComponent.h>
#include <ST/TimeComponent/STTimeComponent.h>




ASTCharacter::ASTCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<USTCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	BaseCharacterMovementComponent = StaticCast<USTCharacterMovementComponent*>(GetCharacterMovement());

	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 1800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<USTInventoryComponent>(TEXT("InventoryComponent"));

	TimeComponent = CreateDefaultSubobject<USTTimeComponent>(TEXT("TimeComponent"));

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ASTCharacter::InitWeapon);
	}

	if (TimeComponent)
	{
		TimeComponent->OnDead.AddDynamic(this, &ASTCharacter::CharDead);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ASTCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	
	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
	TryChangeSprintState(DeltaSeconds);
	TryChangeWalkState(DeltaSeconds);
}

void ASTCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (bIsPlayingDistanceArmLength)
	{
		CameraBoom->TargetArmLength = PlayingDistanceArmLength;
	}
	else
	{
		CameraBoom->TargetArmLength = ProgramDistanceArmLength;
	}

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

UDecalComponent* ASTCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ASTCharacter::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), MoveForwardX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), MoveRightY);

		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel2, true, ResultHit);

			float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				if (!bIsWalkRequested && !bIsSprintRequested)
				{
					Displacement = FVector(0.0f, 0.0f, 120.0f);
				}
				if (bIsWalkRequested)
				{
					Displacement = FVector(0.0f, 0.0f, 120.0f);
				}
				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
			}
		}
	}
}

void ASTCharacter::TryChangeSprintState(float DeltaTime)
{
	if (bIsSprintRequested && !BaseCharacterMovementComponent->IsSprinting() && CanSprint())
	{
		BaseCharacterMovementComponent->StartSprint();
		GetCurrentWeapon()->SetWeaponStateFire(false);

	}
	if (!(bIsSprintRequested && CanSprint()) && BaseCharacterMovementComponent->IsSprinting())
	{
		BaseCharacterMovementComponent->EndSprint();

	}
}

void ASTCharacter::TryChangeWalkState(float DeltaTime)
{
	if (bIsWalkRequested && !BaseCharacterMovementComponent->IsWalking() && CanWalk())
	{
		BaseCharacterMovementComponent->StartWalk();

	}
	if (!(bIsWalkRequested && CanWalk()) && BaseCharacterMovementComponent->IsWalking())
	{
		BaseCharacterMovementComponent->EndWalk();

	}
}

void ASTCharacter::InputAttackPressed()
{
	if (!GetBaseCharacterMovementComponent()->IsSprinting() && bIsAlive)
	{
		AttackCharEvent(true);
	}
}

void ASTCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ASTCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire(bIsFiring);
		
	}
}

void ASTCharacter::StartSprint()
{
	bIsSprintRequested = true;
}

void ASTCharacter::EndSprint()
{
	bIsSprintRequested = false;
}

bool ASTCharacter::CanSprint()
{
	if (FMath::IsNearlyEqual(GetVelocity().Rotation().Yaw, GetActorRotation().Yaw, 30.0f))
	{
		if (bIsCanSprint)
		{
			return true;
		}
		

	}
	return false;
}

void ASTCharacter::StartWalk()
{
	bIsWalkRequested = true;
}

void ASTCharacter::EndWalk()
{
	bIsWalkRequested = false;
}

bool ASTCharacter::CanWalk()
{
	return !GetBaseCharacterMovementComponent()->IsSprinting();
}

AWeaponDefault* ASTCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ASTCharacter::InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	USTGameInstance* myGI = Cast<USTGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{

			if (myWeaponInfo.WeaponClass)
			{

				Sword = myWeaponInfo.IsSword;

				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);

					if (myWeaponInfo.IsSword)
					{
						myWeapon->AttachToComponent(GetMesh(), Rule, FName("SwordSocketRight"));
					
					}
					else
					{
						myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRight"));

					}
					CurrentWeapon = myWeapon;
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->WeaponSetting = myWeaponInfo;

					myWeapon->WeaponInfo = WeaponAdditionalInfo;

					CurrentIndexWeapon = NewCurrentIndexWeapon;


					myWeapon->OnWeaponFireStart.AddDynamic(this, &ASTCharacter::WeaponFireStart);
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ASTCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ASTCharacter::WeaponReloadEnd);

					if (CurrentWeapon->GetWeaponRound() <= 0 && bIsAlive)
					{
						bIsReload = true;
						CurrentWeapon->InitReload();
					}

					if (InventoryComponent)
					{
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
					}
				}

			}
		}
		else
		{

		}
	}
}

void ASTCharacter::TryReloadWeapon()
{
	if (CurrentWeapon && bIsAlive)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && !bIsReload)
		{
			CurrentWeapon->InitReload();
			bIsReload = true;

		}
	}
}

void ASTCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}

	WeaponFireStart_BP(Anim);
}

void ASTCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ASTCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}
	bIsReload = false;
	WeaponReloadEnd_BP(bIsSuccess);
}

void ASTCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{

}

void ASTCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{

}

void ASTCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{

}

void ASTCharacter::TrySwicthNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1 && CurrentWeapon->FireTimer <= 0.0f && bIsAlive)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;

		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
				CurrentWeapon->StartSoundWeapon();
			}
		}
	}
}

void ASTCharacter::TrySwicthPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1 && CurrentWeapon->FireTimer <= 0.0f && bIsAlive)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{

			}
		}
	}
}

void ASTCharacter::CharDead()
{
	float TimeAnim = 0.0f;

	int32 rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}

	bIsAlive = false;

	UnPossessed();

	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ASTCharacter::EnableRandoll, TimeAnim, false);

	GetCursorToWorld()->SetVisibility(false);
	InputAttackReleased();
}

void ASTCharacter::EnableRandoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ASTCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		TimeComponent->ChangeGameTimeValue(-DamageAmount);
		//ChangeHealthValue(-DamageAmount);

	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			//UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());

		}
	}


	return ActualDamage;
}


