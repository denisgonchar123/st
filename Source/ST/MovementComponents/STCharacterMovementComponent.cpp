// Fill out your copyright notice in the Description page of Project Settings.


#include "STCharacterMovementComponent.h"
#include "../Characters/STCharacter.h"

void USTCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseCharacterOwner = Cast<ASTCharacter>(GetOwner());
}

float USTCharacterMovementComponent::GetMaxSpeed() const
{
	float Result = Super::GetMaxSpeed();
	if (bIsSprinting)
	{
		Result = SprintSpeed;

	}
	if (bIsWalking)
	{
		Result = WalkSpeed;

	}

	return Result;
}

void USTCharacterMovementComponent::StartSprint()
{
	bIsSprinting = true;
	bForceMaxAccel = 1;
}

void USTCharacterMovementComponent::EndSprint()
{
	bIsSprinting = false;
	bForceMaxAccel = 0;
}

void USTCharacterMovementComponent::StartWalk()
{
	bIsWalking = true;
}

void USTCharacterMovementComponent::EndWalk()
{
	bIsWalking = false;
}
