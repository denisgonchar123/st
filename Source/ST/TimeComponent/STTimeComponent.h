// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "STTimeComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTimeChange, float, GameTime, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnReward, float, Reward);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ST_API USTTimeComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USTTimeComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnTimeChange OnTimeChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnDead OnDead;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnReward OnReward;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Time")
	float GameTime = 300.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Time")
	float MaxGameTime = 300.0f;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void GameTimeTick(float DeltaTime);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Time")
	bool bIsAlive = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Time")
	float CoefTimeTick = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Time")
	float CoefTime = 1.0f;


	UFUNCTION(BlueprintCallable, Category = "Time")
	float GetCurrentGameTime();

	UFUNCTION(BlueprintCallable, Category = "Time")
	virtual void ChangeGameTimeValue(float ChangeValue, bool bIsTimeProjectile = false, bool bIsReward = false);

	UFUNCTION(BlueprintCallable, Category = "Time")
	void SetCurrentGameTime(float NewHealth);
};
