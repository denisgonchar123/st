// Fill out your copyright notice in the Description page of Project Settings.


#include "STTimeComponent.h"

// Sets default values for this component's properties
USTTimeComponent::USTTimeComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USTTimeComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void USTTimeComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	GameTimeTick(DeltaTime);
}

void USTTimeComponent::GameTimeTick(float DeltaTime)
{
	if (bIsAlive)
	{
		if (GameTime > MaxGameTime)
		{
			GameTime = MaxGameTime;
		
		}
		else
		{
			if (GameTime <= 0.0f )
			{
				GameTime = 0.0f;
				bIsAlive = false;
				OnDead.Broadcast();
			
			} 
			else
			{
				GameTime -= DeltaTime * CoefTimeTick;
			}
		}
		
	}
}

float USTTimeComponent::GetCurrentGameTime()
{
	return GameTime;
}

void USTTimeComponent::ChangeGameTimeValue(float ChangeValue, bool bIsTimeProjectile, bool bIsReward)
{
	if (bIsAlive)
	{
		if (bIsTimeProjectile)
		{
			ChangeValue = ChangeValue * CoefTime;

		}
		
		GameTime += ChangeValue;
		if (bIsReward)
		{
			OnReward.Broadcast(ChangeValue);
		}


		if (GameTime > MaxGameTime)
		{
			GameTime = MaxGameTime;

		}
		else
		{
			if (GameTime <= 0.0f)
			{
				GameTime = 0.0f;
				bIsAlive = false;
				OnDead.Broadcast();

			}
		}
		

		OnTimeChange.Broadcast(GameTime, ChangeValue);
	}
}

void USTTimeComponent::SetCurrentGameTime(float NewHealth)
{
	GameTime = NewHealth;
}

