// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include <ST/Characters/STCharacter.h>
#include "STPlayerController.generated.h"

UCLASS()
class ASTPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ASTPlayerController();

	virtual void SetPawn(APawn* InPawn) override;

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	
	void SetNewMoveDestination(const FVector DestLocation);

	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

	TSoftObjectPtr<class ASTCharacter> CachedBaseCharacter;

public:
	void MoveForward(float Value);
	void MoveRight(float Value);
	//Fire
	void TryStartFire();
	void TryEndFire();
	//Sprint
	void StartSprint();
	void EndSprint();
	//Walk
	void StartWalk();
	void EndWalk();

	//Reload 
	void ReloadWeapon();

	//Switch
	void SwitchNextWeapon();
	void SwitchPreviosWeapon();
};


