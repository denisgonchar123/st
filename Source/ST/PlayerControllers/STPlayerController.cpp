// Copyright Epic Games, Inc. All Rights Reserved.

#include "STPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "../Characters/STCharacter.h"
#include "Engine/World.h"

ASTPlayerController::ASTPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ASTPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	CachedBaseCharacter = Cast<ASTCharacter>(InPawn);
}

void ASTPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

}

void ASTPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &ASTPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ASTPlayerController::MoveRight);

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &ASTPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &ASTPlayerController::OnSetDestinationReleased);
	InputComponent->BindAction("Sprint", IE_Pressed, this, &ASTPlayerController::StartSprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &ASTPlayerController::EndSprint);
	InputComponent->BindAction("Walk", IE_Pressed, this, &ASTPlayerController::StartWalk);
	InputComponent->BindAction("Walk", IE_Released, this, &ASTPlayerController::EndWalk);
	InputComponent->BindAction("Fire", IE_Pressed, this, &ASTPlayerController::TryStartFire);
	InputComponent->BindAction("Fire", IE_Released, this, &ASTPlayerController::TryEndFire);
	InputComponent->BindAction("Reload", IE_Pressed, this, &ASTPlayerController::ReloadWeapon);
	InputComponent->BindAction("SwitchNextWeapon", IE_Pressed, this, &ASTPlayerController::SwitchNextWeapon);
	InputComponent->BindAction("SwitchPreviosWeapon", IE_Pressed, this, &ASTPlayerController::SwitchPreviosWeapon);

}


void ASTPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if ((Distance > 120.0f))
		{
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void ASTPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void ASTPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void ASTPlayerController::MoveForward(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->MoveForward(Value);
	}
}

void ASTPlayerController::MoveRight(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->MoveRight(Value);
	}
}

void ASTPlayerController::TryStartFire()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->InputAttackPressed();
	}
}

void ASTPlayerController::TryEndFire()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->InputAttackReleased();
	}
}

void ASTPlayerController::StartSprint()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->StartSprint();
	}
}

void ASTPlayerController::EndSprint()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->EndSprint();
	}
}

void ASTPlayerController::StartWalk()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->StartWalk();
	}
}

void ASTPlayerController::EndWalk()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->EndWalk();
	}
}

void ASTPlayerController::ReloadWeapon()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->TryReloadWeapon();
	}
}

void ASTPlayerController::SwitchNextWeapon()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->TrySwicthNextWeapon();
	}
}

void ASTPlayerController::SwitchPreviosWeapon()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->TrySwicthPreviosWeapon();
	}
}

