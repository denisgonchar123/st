// Fill out your copyright notice in the Description page of Project Settings.


#include "STHealthComponent.h"

// Sets default values for this component's properties
USTHealthComponent::USTHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USTHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void USTHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float USTHealthComponent::GetCurrentHealth()
{
	return Health;
}

void USTHealthComponent::ChangeHealthValue(float ChangeValue)
{
	ChangeValue = ChangeValue * CoefDamage;

	Health += ChangeValue;

	if (Health > HealthMax)
	{
		Health = HealthMax;
	}
	else
	{

		if (Health <= 0.0f)
		{
			OnHealthDead.Broadcast();

		}
	}

	OnHealthChange.Broadcast(Health, ChangeValue);
}

void USTHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

