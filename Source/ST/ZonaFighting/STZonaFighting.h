// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STZonaFighting.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnZonaFightingSwitchedSignature, bool);


UCLASS()
class ST_API ASTZonaFighting : public AActor
{
	GENERATED_BODY()
	
public:	
	
	FOnZonaFightingSwitchedSignature OnZonaFightingSwitchedSignature;

protected:
	
	virtual void BeginPlay() override;

public:	
	
	UFUNCTION(BlueprintCallable)
	void SwitchZonaFight();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "State")
	bool bIsOn = true;



};
