// Fill out your copyright notice in the Description page of Project Settings.


#include "STZonaFighting.h"


// Called when the game starts or when spawned
void ASTZonaFighting::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASTZonaFighting::SwitchZonaFight()
{
	bIsOn = !bIsOn;

	if (OnZonaFightingSwitchedSignature.IsBound())
	{
		OnZonaFightingSwitchedSignature.Broadcast(bIsOn);
	}

}


